import {AbstractControl, ValidatorFn} from '@angular/forms';

export function costValueValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = isNaN(control.value) || Number.parseInt(control.value) < 0;
    return forbidden ? {'forbiddenName': {value: control.value}} : null;
  };
}
