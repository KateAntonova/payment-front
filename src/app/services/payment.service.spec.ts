import {PaymentService} from './payment.service';
import {TestBed} from '@angular/core/testing';

describe('PaymentService', function(){
  let service: PaymentService;
  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [PaymentService] });
    service = TestBed.get(PaymentService);
  });
  it('should return define value', () => {
    expect(service.getPaymentList()).toBeDefined();
  });
});
