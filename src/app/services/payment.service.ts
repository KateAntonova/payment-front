import {Injectable} from '@angular/core';
import {IPayment} from '../models/payment.model';

@Injectable()
export class PaymentService {

  getPaymentList(): IPayment[] {
    return [
      {
        handle: 'payment 1',
        cost: 111,
        month: [true, false, false, false, false, false, false, false, false, false, false, false]
      },
      {
        handle: 'payment 2',
        cost: 222,
        month: [true, true, false, false, false, false, false, false, false, false, false, false]
      },
      {
        handle: 'payment 3',
        cost: 333,
        month: [true, true, true, false, false, false, false, false, false, false, false, false]
      }
    ];
  }
}
