import {Component, OnInit} from '@angular/core';
import {IPayment} from '../../models/payment.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {costValueValidator} from '../../directives/cost-validator.directive';
import {PaymentService} from '../../services/payment.service';
import {monthRU} from '../../../month.const';
import {monthEN} from '../../../month.const';

@Component({
  selector: 'app-payment-list',
  templateUrl: 'payment-list.component.html',
  styleUrls: ['payment-list.component.css']
})
export class PaymentListComponent implements OnInit {

  totalCost = 0;
  paymentList: IPayment[] = [];
  monthRU = monthRU;
  monthEN = monthEN;
  displayedColumns: string[] = ['handle', 'cost', ...this.monthEN, 'delete'];

  paymentForm: FormGroup;

  constructor(private _fb: FormBuilder,
              private _paymentService: PaymentService) {
  }

  ngOnInit() {
    this.paymentList = this._paymentService.getPaymentList();
    this.paymentForm = this._fb.group({
      handle: ['', Validators.required],
      cost: ['', costValueValidator()]
    });
    this.calculateTotalCost();
  }

  deletePayment(handle) {
    this.paymentList = [
      ...this.paymentList
        .filter(elem => (handle !== elem.handle) ? elem : null)
    ];
    this.calculateTotalCost();
  }

  onSubmit() {
    if (this.paymentForm.invalid) {
      console.log('form invalid');
    } else {
      this.paymentList = [...this.paymentList, {
        handle: this.paymentForm.get('handle').value,
        cost: this.paymentForm.get('cost').value || 0,
        month: [false, false, false, false, false, false, false, false, false, false, false, false]
      }];
      this.paymentForm.reset();
      Object.keys(this.paymentForm.controls).forEach((name) => {
        const control = this.paymentForm.controls[name];
        control.setErrors(null);
      });
      console.log(this.paymentForm);
    }
  }

  calculateTotalCost() {
    this.totalCost = 0;
    this.paymentList
      .forEach((payment: IPayment) => {

        let selectedMonthCount = 0;
        payment
          .month
          .forEach((month: boolean) => {
            selectedMonthCount += month ? 1 : 0;
          });
        this.totalCost += selectedMonthCount * payment.cost;
      });

  }
}
