import {TestBed, async} from '@angular/core/testing';
import {PaymentListComponent} from './payment-list.component';
import {MaterialComponent} from '../../material.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PaymentService} from '../../services/payment.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

describe('PaymentListComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PaymentListComponent
      ],
      imports: [
        MaterialComponent,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
      ],
      providers: [
        PaymentService
      ]
    }).compileComponents();
  }));
  it('should create the totalCost', async(() => {
    const component = TestBed.createComponent(PaymentListComponent);
    const totalCost = component.debugElement.componentInstance;
    expect(totalCost).toBeTruthy();
  }));
  it('should create the paymentList', async(() => {
    const component = TestBed.createComponent(PaymentListComponent);
    const paymentList = component.debugElement.componentInstance;
    expect(paymentList).toBeTruthy();
  }));
  it('should create the displayedColumns', async(() => {
    const component = TestBed.createComponent(PaymentListComponent);
    const displayedColumns = component.debugElement.componentInstance;
    expect(displayedColumns).toBeTruthy();
  }));
  it('should create the paymentForm', async(() => {
    const component = TestBed.createComponent(PaymentListComponent);
    const paymentForm = component.debugElement.componentInstance;
    expect(paymentForm).toBeTruthy();
  }));
  it('should render text in a p tag', async(() => {
    const component = TestBed.createComponent(PaymentListComponent);
    component.detectChanges();
    const compiled = component.debugElement.nativeElement;
    expect(compiled.querySelector('p').textContent).toContain('Израсходовано средств:');
  }));

});
