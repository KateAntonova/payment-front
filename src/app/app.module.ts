import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {PaymentListComponent} from './pages/payment-list/payment-list.component';
import {AppRoutes} from './app.routes';
import {MaterialComponent} from './material.component';
import {PaymentService} from './services/payment.service';


@NgModule({
  declarations: [
    AppComponent,
    PaymentListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutes,
    MaterialComponent,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [PaymentService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
