import {RouterModule, Routes} from '@angular/router';
import {PaymentListComponent} from './pages/payment-list/payment-list.component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/payment',
    pathMatch: 'full'
  },
  {
    path: 'payment',
    component: PaymentListComponent
  },
  {
    path: '**',
    redirectTo: '/payment'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutes {

}
