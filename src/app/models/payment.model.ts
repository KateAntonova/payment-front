export interface IPayment {
  id?: string;
  handle: string;
  cost: number;
  month: boolean[];
}
